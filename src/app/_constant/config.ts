import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { ModalOptions } from 'ngx-bootstrap';
import { Constant } from './constant';

export class Config {
	public static THEME = Constant.background.light;
	public static MANU_THEME = Constant.background.danger;
	public static MANU_ACTIVE = "text-danger";
	public static LOADING_CONFIG = {backdropBackgroundColour: 'rgba(0, 0, 0, 0.6)'
                , animationType: ngxLoadingAnimationTypes.wanderingCubes
                , primaryColour: '#4e73df'
				, secondaryColour: '#e74a3b'};
	public static DATATABLE_CONFIG = {
		pagingType: 'full_numbers',
		ordering: false,
		scrollX: false,
		lengthChange: false,
		info: false,
		pageLength: 20,
		searching: false,
		processing: true,
		serverSide: false
	};
	public static MODAL_CONFIG: ModalOptions = {
					backdrop : 'static',
					keyboard : false };
}