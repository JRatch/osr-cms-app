export class Constant {
    public static  background = { primary : 'bg-primary'
					, info: 'bg-info'
					, success: 'bg-success'
					, warning: 'bg-warning'
					, danger: 'bg-danger'
					, dark : 'bg-dark'
					, secondary : 'bg-secondary'
                    , white : 'bg-white'
                    , light: 'bg-light'
                    };
    public static color = {
                    primary : "#337ab7"
                    , info: '#659be0'
					, success: '#36c6d3'
                    , warning: '#F1C40F'
                    , danger: '#ed6b75'
    }
    public static typeChart = { bar : 'bar'
                    , horizontalBar : 'horizontalBar'
                    , line: 'line'
                    , pie: 'pie'
                    , radar: 'radar'
                    , doughnut: 'doughnut'
                    , polarArea: 'polarArea'
                    , bubble: 'bubble'
                    , scatter: 'scatter'
                    };
}