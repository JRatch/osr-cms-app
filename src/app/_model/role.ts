export class Role {
    username: string;
    authorities: string[];
    organizeCode: string;
    organizeDesc: string;
    token: string;
    fullName: string;
}