import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit , AfterViewInit{
 
  public HEADER:string = 'Home Page';
  public BREADCRUMB : any = [{label : 'Home Page' , link : '/homePage'}];
  constructor(private toastr: ToastrService) { }

  ngOnInit() {
    
  }

  ngAfterViewInit(): void {
  }

}
