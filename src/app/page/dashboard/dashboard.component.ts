import { Component, OnInit } from '@angular/core';
import { Constant } from 'src/app/_constant/constant';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public dataChart : any = null;
  public dataChart1 : any = null;
  constructor() { }

  ngOnInit() {
    this.dataChart = {
      datasets: [{
        data: [10, 20, 30], 
        backgroundColor: [Constant.color.danger, Constant.color.warning, Constant.color.primary ],
        minBarLength: 0
      }],
      labels: [
          'Red',
          'Yellow',
          'Blue'
      ]
    }

    this.dataChart1 = {
      datasets: [{
        label: "red",
        data: [10],
        backgroundColor:  Constant.color.danger
      },{
        label: "yellow",
        data: [20],
        backgroundColor:  Constant.color.warning
      },{
        label: "blue",
        data: [30],
        backgroundColor:  Constant.color.primary
      }]
    }
  }

}
