import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DataTablesModule } from 'angular-datatables';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { ProductSetupComponent } from './productSetup.component';
import { ComponentModule } from 'src/app/_component/component.module';

const routes: Routes = [
  { path: '', component: ProductSetupComponent }
];

@NgModule({
  declarations: [ProductSetupComponent],
  imports: [
    CommonModule,
    DataTablesModule,
    RouterModule.forChild(routes),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    ComponentModule
  ],
  exports: [RouterModule],
})
export class ProducSetupModule { }
