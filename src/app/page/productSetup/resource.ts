export class ProductSetupResource {
    public static  tableHeader = [  { text:'#', class:'text-center'}
                                    ,{ text:'Product Code', class:'text-center'}
                                    ,{ text:'Plan Group Type', class:'text-center'}
                                    ,{ text:'Description', class:'text-center'}];
    public static  tableValue = [{column1: "10TLA"
                                    , column2: "Basic"
                                    , column3: "Term Life 10 (10TLA)" },
                                {column1: "WP"
                                    , column2: "Basic"
                                    , column3: "Waiver of Premium" },
                                {column1: "E2015"
                                    , column2: "Basic"
                                    , column3: "Endowment 20/15 (E2015)" },
                                {column1: "HSD05"
                                    , column2: "Rider"
                                    , column3: "Hospital Room, Board and Nursing Charge (Maximum 150 days – ICU Room included) 500 Baht / Day" },
                                {column1: "HBF"
                                    , column2: "Rider"
                                    , column3: "Hospital Benefit Fit" }];
}