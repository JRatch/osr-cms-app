import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { Config } from 'src/app/_constant/config';
import { DataTableDirective } from 'angular-datatables';
import { ProductSetupResource } from './resource';

@Component({
  selector: 'app-productSetup',
  templateUrl: './productSetup.component.html',
  styleUrls: ['./productSetup.component.css']
})
export class ProductSetupComponent implements OnInit , AfterViewInit{
  public dtOptions :DataTables.Settings = null;
  public dtTrigger: Subject<any> = new Subject();
  public HEADER:string = 'Product Setup';
  public tableHeader :any = null;
  public valueList: any = [];
  @ViewChild(DataTableDirective,{static: false}) dtElement: DataTableDirective;
  constructor(private toastr: ToastrService) { 
    this.dtOptions = Config.DATATABLE_CONFIG;
    
  }

  ngOnInit() {
    this.tableHeader = ProductSetupResource.tableHeader;
    this.valueList = ProductSetupResource.tableValue;
  }

  ngAfterViewInit(): void {
  }

  rerender(): void {
    // this.dtTrigger.next();
    if(this.dtElement){
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        //   // Destroy the table first
          dtInstance.destroy();
        //   // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
    }
    
  }
}
