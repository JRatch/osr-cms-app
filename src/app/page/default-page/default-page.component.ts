import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-default-page',
  templateUrl: './default-page.component.html',
  styleUrls: ['./default-page.component.css']
})
export class DefaultPageComponent implements OnInit , AfterViewInit{
 
  public HEADER:string = 'Default Page';
  public BREADCRUMB : any = [{label : 'Default Page' , link : '/defaultPage'}];
  constructor(private toastr: ToastrService) { }

  ngOnInit() {
    
  }

  ngAfterViewInit(): void {
  }

}
