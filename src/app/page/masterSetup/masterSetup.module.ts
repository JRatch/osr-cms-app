import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DataTablesModule } from 'angular-datatables';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { ComponentModule } from 'src/app/_component/component.module';
import { ChannelComponent } from './channelMaster/channel.component';
import { DataCollectorComponent } from './DataCollectorMaster/dataCollector.component';

const routes: Routes = [
  { path: '', component: ChannelComponent },
  { path: 'chanelMaster', component: ChannelComponent },
  { path: 'dataCollectorMaster', component: DataCollectorComponent }
  
];

@NgModule({
  declarations: [ChannelComponent
                , DataCollectorComponent],
  imports: [
    CommonModule,
    DataTablesModule,
    RouterModule.forChild(routes),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    ComponentModule
  ],
  exports: [RouterModule],
})
export class MasterSetupModule { }
