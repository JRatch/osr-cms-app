export class DataCollectorMasterResource {
    public static  tableHeader = [  { text:'#', class:'text-center'}
                                    ,{ text:'Data Collector', class:'text-center'}
                                    ,{ text:'Description', class:'text-center'}
                                    ,{ text:'Action', class:'text-center'}];
    public static  tableValue = [{column1: "DC 1 POS"
                                    , column2: "POS Information"
                                    , column3: "1" },
                                {column1: "DC 2 Claim"
                                    , column2: "Claim history"
                                    , column3: "2" },
                                {column1: "DC 3 Product Info"
                                    , column2: "Product master"
                                    , column3: "3" },
                                {column1: "DC 4 PB POS"
                                    , column2: "PB POS Information"
                                    , column3: "4" },
                                {column1: "DC 5 PB Claim"
                                    , column2: "PB Claim history"
                                    , column3: "5" },
                                {column1: "DC 6 Product Attach"
                                    , column2: "Product Attachment"
                                    , column3: "6" }];
}