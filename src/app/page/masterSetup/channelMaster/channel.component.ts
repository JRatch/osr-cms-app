import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { Config } from 'src/app/_constant/config';
import { DataTableDirective } from 'angular-datatables';
import { ChannelMasterResource } from './resource';

@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.css']
})
export class ChannelComponent implements OnInit , AfterViewInit{
  public dtOptions :DataTables.Settings = null;
  public dtTrigger: Subject<any> = new Subject();
  public HEADER:string = 'Channel Master';
  public tableHeader :any = null;
  public valueList: any = [];
  public defaultModal: boolean =false;
  @ViewChild(DataTableDirective,{static: false}) dtElement: DataTableDirective;
  constructor(private toastr: ToastrService) { 
    this.dtOptions = Config.DATATABLE_CONFIG;
    
  }

  ngOnInit() {
    this.tableHeader = ChannelMasterResource.tableHeader;
    this.valueList = ChannelMasterResource.tableValue;
  }

  ngAfterViewInit(): void {
  }
  showModal(value:boolean){
    this.defaultModal = value;
  }
  rerender(): void {
    // this.dtTrigger.next();
    if(this.dtElement){
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        //   // Destroy the table first
          dtInstance.destroy();
        //   // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
    }
    
  }
}
