import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { Config } from 'src/app/_constant/config';
import { DataTableDirective } from 'angular-datatables';
@Component({
  selector: 'c-dataTable',
  templateUrl: './dataTable.component.html',
  styleUrls: ['./dataTable.component.css']
})
export class DataTableComponent implements OnInit {
  public header: any = null
  @Input() dtOptions :DataTables.Settings = null;
  public dtTrigger: Subject<any> = new Subject();
  public tableValue :any = [];
  @ViewChild(DataTableDirective,{static: false}) dtElement: DataTableDirective;
  constructor() { }
  ngOnInit() {
    if(this.dtOptions == null){
      this.dtOptions = Config.DATATABLE_CONFIG;
    }
  }
  @Input()
	set tableHeader(tableHeder) {
    this.header = tableHeder;
    console.log("header", this.header);
		this.rerender();
	}
  @Input()
	set value(list) {
    this.tableValue = list;
		this.rerender();
	}
  rerender(): void {
    // this.dtTrigger.next();
    if(this.dtElement){
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        //   // Destroy the table first
          dtInstance.destroy();
        //   // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
    }
    
  }
  
}
