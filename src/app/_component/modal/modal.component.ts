import { Component, Input, OnInit, ViewChild, ElementRef, Output ,EventEmitter} from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { Config } from 'src/app/_constant/config';
 
@Component({
  selector: 'c-modal',
  templateUrl: './modal.component.html'
})
export class ModalComponent implements OnInit {
	@ViewChild('modalalert' ,{static: false}) mymodal: ElementRef;
	public modalRef: BsModalRef;

	@Input() typeModal: string = '';
	@Input() id: string = "modal1";
	@Output() onSubmit: EventEmitter<any> = new EventEmitter();
	@Input() addHeader: string = '';
	@Input() addBody: string = '';
	
	public addIcon :string = "";
	public border :string = "";
	public colorIcon :string = "";
  constructor(private modalService: BsModalService) {
		
	}
	ngOnInit(): void {
		switch (this.typeModal) {
			case "success":
				this.addIcon = 'fa-check';
				this.border = 'border-left-success';
				this.colorIcon = 'text-success';
				this.addHeader = this.addHeader ? this.addHeader : "เสร็จสิ้น";
				this.addBody = this.addBody ? this.addBody : "";
				break;
			case "error":
				this.addIcon = 'fa-times'
				this.border = 'border-left-danger'
				this.colorIcon = 'text-danger';
				this.addHeader = this.addHeader ? this.addHeader : "เกิดข้อผิดพลาด";
				this.addBody = this.addBody ? this.addBody : "";
				break;
			case "warning":
				this.addIcon = 'fa-exclamation';
				this.border = 'border-left-warning';
				this.colorIcon = 'text-warning';
				this.addHeader = this.addHeader ? this.addHeader : "แจ้งเตือน";
				this.addBody = this.addBody ? this.addBody : "";
				break;
			default:
				this.addIcon = 'fa-bell-o';
				this.border = 'border-left-primary';
				this.colorIcon = 'text-primary';
				this.addHeader = this.addHeader ? this.addHeader : "ยืนยัน";
				this.addBody = this.addBody ? this.addBody : "";
				break;
		}
	}
	ngAfterViewInit(): void {
		// this.openModal();
	}
	@Input()
	set isShow(isShow: boolean) {
		if(isShow){
			this.openModal();
		}else{
			if(this.modalRef){
				this.modalRef.hide();
			}
		}
	}
	openModal() {
		let cobfig = Config.MODAL_CONFIG;
		this.modalRef = this.modalService.show(this.mymodal ,cobfig);
	}
	clickSubmit(){
		this.onSubmit.emit();
	}
}