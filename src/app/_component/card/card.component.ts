import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'c-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
	@Input() addHeader: string = '';
  constructor() { }
  ngOnInit() {
  }

}
