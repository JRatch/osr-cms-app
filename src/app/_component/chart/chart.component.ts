import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'c-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {
  @Input() typeChart: any = null;
  @Input() dataChart: any = null;
  
  optionsChart: any;
  
  constructor() { 
    
    
  }
  ngOnInit() {
    this.optionsChart = {
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        xAxes: [{
          ticks: {
            beginAtZero: true,
            suggestedMin: 5
          }
        }]
      },
    };
  }

}
