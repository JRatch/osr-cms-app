import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'c-form-group',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {
  @Input() addWording: string = '';
  @Input() isRequired: boolean = false;
  @Input() isRow: boolean = false;
  constructor() { }

  ngOnInit() {

  }

}
