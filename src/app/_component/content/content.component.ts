import { Component, OnInit, Input } from '@angular/core';
import { Config } from 'src/app/_constant/config';

@Component({
  selector: 'c-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
	@Input() header: string = 'header';
  @Input() isBreadcrumb:boolean = true;
  @Input() breadcrumb:any = [];
  MENU_ACTIVE: string ='';
  constructor() { 
    this.MENU_ACTIVE = Config.MANU_ACTIVE;
  }
  ngOnInit() {

  }

}
