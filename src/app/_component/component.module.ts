import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalComponent } from './modal/modal.component';
import { ModalModule } from 'ngx-bootstrap';
import { ContentComponent } from './content/content.component';
import { RouterModule } from '@angular/router';
import { ButtonComponent } from './button/button.component';
import { InputComponent } from './input/input.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardComponent } from './card/card.component';
import { DataTableComponent } from './dataTable/dataTable.component';
import { DataTablesModule } from 'angular-datatables';
import { ChartComponent } from './chart/chart.component';
import { ChartModule } from 'angular2-chartjs';
@NgModule({
  declarations: [
    ModalComponent,
    ContentComponent,
    ButtonComponent,
    CardComponent,
    DataTableComponent,
    ChartComponent,
    InputComponent,
    
  ],
  imports: [
    CommonModule,
    ModalModule.forRoot(),
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    ChartModule
    // BrowserAnimationsModule
  ],
  exports: [
    ModalComponent,
    ContentComponent,
    ButtonComponent,
    CardComponent,
    DataTableComponent,
    ChartComponent,
    InputComponent
  ]
})
export class ComponentModule { }
