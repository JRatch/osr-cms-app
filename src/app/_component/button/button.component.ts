import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'c-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {
  @Input() addWording: string = '';
  @Input() addClass: string = '';
  @Input() addIcon: string = '';
  @Input() typeButton: string = '';
  @Input() disabled: boolean = false;
  constructor() { }

  ngOnInit() {
    switch (this.typeButton) {
      case 'add':
        this.addWording = this.addWording ? this.addWording:'เพิ่มข้อมูล';
        this.addClass = this.addClass ? this.addClass :'btn-primary btn-icon-split';
        this.addIcon = this.addIcon ? this.addIcon :'fa-plus';
        break;
      case 'edit':
        this.addWording = this.addWording ? this.addWording: 'แก้ไขข้อทูล';
        this.addClass = this.addClass ? this.addClass :'btn-warning btn-icon-split';
        this.addIcon = this.addIcon ? this.addIcon :'fa-edit';
        break;
      case 'info':
          this.addWording = this.addWording ? this.addWording: 'รายละเอียด';
          this.addClass = this.addClass ? this.addClass :'btn-info btn-icon-split';
          this.addIcon = this.addIcon ? this.addIcon :'fa-eye';
        break;
      case 'delete':
        this.addWording = this.addWording ? this.addWording: 'ลบข้อมูล';
        this.addClass = this.addClass ? this.addClass :'btn-danger btn-icon-split';
        this.addIcon = this.addIcon ? this.addIcon :'fa-minus';
        break;
      case 'submit':
        this.addWording = this.addWording ? this.addWording: 'ยืนยัน';
        this.addClass = this.addClass ? this.addClass :'btn-success btn-icon-split';
        this.addIcon = this.addIcon ? this.addIcon :'fa-check';
        break;
      case 'cancel':
        this.addWording = this.addWording ? this.addWording: 'ยกเลิก';
        this.addClass = this.addClass ? this.addClass : 'btn-secondary btn-icon-split';
        this.addIcon = this.addIcon ? this.addIcon :'fa-times';
        break;
      case 'icon':
        this.addWording = '';
        this.addClass = this.addClass ? this.addClass :'';
        this.addIcon = this.addIcon ? this.addIcon : '';
        break;
      case 'search':
        this.addWording = this.addWording ? this.addWording: 'ค้นหา';
        this.addClass = this.addClass ? this.addClass : 'btn-danger btn-icon-split';
        this.addIcon = this.addIcon ? this.addIcon :'fa-search';
        break;
      default:
        break;
    } //end of switch
  }

}
