import { Component } from '@angular/core';
import { Globals } from './_global/globals';
import { Config } from './_constant/config';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'RachTemp';
  public loadingConfig = Config.LOADING_CONFIG;
  constructor(public globals: Globals) {}
}
