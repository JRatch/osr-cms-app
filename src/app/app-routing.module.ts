import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './_guard/auth.guard';
import { LoginComponent } from './_common/login/login.component';


const routes: Routes = [
  { path: 'login' , component: LoginComponent},
  { path: '', loadChildren: './_common/layout/layout.module#LayoutModule', canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
