import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/_service/user.service.';
import { User } from 'src/app/_model/user';
import { Config } from 'src/app/_constant/config';
declare var $: any;
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})

export class LayoutComponent implements OnInit {
  isMenu : boolean = true;
  user: User = null;
  BASE_THEME: string = '';
  MENU_THEME: string = '';
  MENU_ACTIVE: string ='';
  constructor(
    private router: Router,
    private userSV: UserService,
  ) {
    this.BASE_THEME = Config.THEME;
    this.MENU_THEME = Config.MANU_THEME;
    this.MENU_ACTIVE = Config.MANU_ACTIVE;
    // common.loading();

    // setTimeout(() => {
    //   /** spinner ends after 5 seconds */
    //   this.common.unLoading();
    // }, 2000);
  }


  ngOnInit() {
    this.user = new User;
    this.user.fullName = this.userSV.currentUserValue.fullName;
    this.user.token = this.userSV.currentUserValue.token;
    this.user.organizeCode = this.userSV.currentUserValue.organizeCode;
    this.user.organizeDesc = this.userSV.currentUserValue.organizeDesc;
    this.user.authorities = this.userSV.currentUserValue.authorities;
    this.user.username = this.userSV.currentUserValue.username;

    $(document).ready(function () {
      $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
        $(".dataTables_scrollHeadInner").css({ "width": "100%" });
        $(".table ").css({ "width": "100%" });
      });
    });
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    
  }
  onShowManu(){
    this.isMenu = ! this.isMenu;
  }
  otherClick() {
    $(document).ready(function () {
      $('#sidebar').addClass('active');
      $(".dataTables_scrollHeadInner").css({ "width": "100%" });
      $(".table ").css({ "width": "100%" });
    });
  }
  
  scrollToTop() {
    window.scroll(0, 0);
  }
  mainContentClick(){
    this.isMenu = false;
  }
  toTop() {

  }



}
