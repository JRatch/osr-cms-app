import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { DashboardComponent } from 'src/app/page/dashboard/dashboard.component';
import { DefaultPageComponent } from 'src/app/page/default-page/default-page.component';
import { HomeComponent } from 'src/app/page/home/home.component';

const routes: Routes = [
  {
    path: '', component: LayoutComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'home', component: DashboardComponent },
      { path: 'defaultPage', component: DefaultPageComponent },
      { path: 'productSetup', loadChildren: '../../page/productSetup/productSetup.module#ProducSetupModule' },
      { path: 'master', loadChildren: '../../page/masterSetup/masterSetup.module#MasterSetupModule' },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
