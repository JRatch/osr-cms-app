import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { ComponentModule } from 'src/app/_component/component.module';
import { DashboardComponent } from 'src/app/page/dashboard/dashboard.component';
import { DefaultPageComponent } from 'src/app/page/default-page/default-page.component';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { HomeComponent } from 'src/app/page/home/home.component';


@NgModule({
  declarations: [
    LayoutComponent,
    DashboardComponent,
    DefaultPageComponent,
    HomeComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    LayoutRoutingModule,
    ComponentModule,
    DataTablesModule
  ]
})
export class LayoutModule { }
