import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AjaxService } from 'src/app/_service/ajax.service';
import { UserService } from 'src/app/_service/user.service.';
import { User } from 'src/app/_model/user';
import { ValidateService } from 'src/app/_service/validate.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  public username:string = "";
  public password:string = "";
  public user: User = new User;

  constructor(private router: Router
    , private ajax: AjaxService
    , private validate: ValidateService
    , private userSV: UserService) { }

  ngOnInit() {
    this.userSV.logout();
    
  }

  login() {
    let param = { username: this.username, password: this.password };
    if(param.username == "admin" && param.password == "password"){
      this.user.fullName = "นายบัญชา ภุมรินทร์";
      this.user.token = "asda49103uwhjfuy32847sdkljvs";
      this.user.organizeCode = "000";
      this.user.organizeDesc = "Aventure";
      this.user.authorities = ["admin"] ;
      this.user.username = "admin";
      this.userSV.logIn(this.user);
      this.router.navigate(['dashboard']);
    }else{
      let data =  [{header:"username", value: param.username}
                    , {header:"password" , value: param.password}]
      if(this.validate.checking(data)){
        this.ajax.doPostLogin("token/generate-token", param).subscribe((response) => {
          if (response) {
            if(!response.authorities){
              // this.bodyModal = "ไม่มีสิทธิ์การเข้าใช้งาน";
              this.userSV.logout();
              // this.modalError.openModal();
            }else{
                let authoritiesList = response.authorities.split(",");
                this.user.fullName = response.fullName;
                this.user.token = response.token;
                this.user.organizeCode = response.organizeCode;
                this.user.organizeDesc = response.organizeDesc;
                this.user.authorities = authoritiesList ;
                this.user.username = response.username;
                this.userSV.logIn(this.user);

                this.router.navigate(['/dashboard']);
              }
              
            } else {
              this.userSV.logout();
            }// else
          }, // response
          error => 
          {
            this.userSV.logout();
            // this.modalError.openModal();
          }
        ) ;// subscribe
      }
    }
  }
}
