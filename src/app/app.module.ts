import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AlertModule } from 'ngx-bootstrap';
import { ComponentModule } from './_component/component.module';
import { UserService } from './_service/user.service.';
import { AjaxService } from './_service/ajax.service';
import { LoginComponent } from './_common/login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { NgxLoadingModule } from 'ngx-loading';
import { Globals } from './_global/globals';
import {  ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { ValidateService } from './_service/validate.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgxLoadingModule.forRoot({}),
    ComponentModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({timeOut: 3000, positionClass: 'toast-bottom-right'})
  ],
  providers: [
    UserService,
    AjaxService,
    ValidateService,
    Globals
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
