import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Router } from '@angular/router';
import { empty, throwError, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { UserService } from './user.service.';
import { Globals } from '../_global/globals';

declare var apiPath: string;

@Injectable()
export class AjaxService {

  public static CONTEXT_PATH = apiPath + "/api/";
  public static CONTEXT_PATH_EXPORT = apiPath + "/export/";
  public static CONTEXT_PATH_LOGIN = apiPath+"/";
  public static isDebug = false;
  private httpOptions: any;
  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private user: UserService,
    private globals: Globals
  ) {
    console.log("apiPath", apiPath);
    // console.log("user token "+ user.getToken());
    // console.log("user.currentUserValue",user.currentUserValue.token);

  }

  private handleError(error: any): Promise<any> {
    console.error("An error occurred", error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  private doHandleError = (error: any) => {
    
    this.globals.loading = false;
    if(error.status == 401){
      this.router.navigate(['/login']);
    }
    return Promise.reject(error.message || error);
 }

  doPost(url: string, body: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${this.user.currentUserValue.token}`
      })
    };
    if (AjaxService.isDebug) {
      console.log("URL : ", AjaxService.CONTEXT_PATH + url);
      console.log("Params : ", body);
    }
    this.globals.loading = true;
    return this.httpClient.post(AjaxService.CONTEXT_PATH + url, body, httpOptions).pipe(
      map((response: any) => {
        this.globals.loading = false;
        return response;
      }),
      catchError(this.doHandleError)
    );
  }

  doGet(url: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${this.user.currentUserValue.token}`
      })
    };
    if (AjaxService.isDebug) {
      console.log("URL : ", AjaxService.CONTEXT_PATH + url);
    }
    this.globals.loading = true;
    return this.httpClient.get(AjaxService.CONTEXT_PATH + url, httpOptions).pipe(
      map((response: any) => {
        this.globals.loading = false;
        return response;
      }),
      catchError(this.doHandleError)
    );
  }

  doPut(url: string, body: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${this.user.currentUserValue.token}`
      })
    };
    if (AjaxService.isDebug) {
      console.log("URL : ", AjaxService.CONTEXT_PATH + url);
      console.log("Params : ", body);
    }
    this.globals.loading = true;
    return this.httpClient.put(AjaxService.CONTEXT_PATH + url, body, httpOptions).pipe(
      map((response: any) => {
        this.globals.loading = false;
        return response;
      }),
      catchError(this.doHandleError)
    );
  }

  doDelete(url: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${this.user.currentUserValue.token}`
      })
    };
    if (AjaxService.isDebug) {
      console.log("URL : ", AjaxService.CONTEXT_PATH + url);
    }
    this.globals.loading = true;
    return this.httpClient.delete(AjaxService.CONTEXT_PATH + url, httpOptions).pipe(
      map((response: any) => {
        this.globals.loading = false;
        return response;
      }),
      catchError(this.doHandleError)
    );
  }

  upload(url: string, body: any, success: any, error?: any, header?: Headers) {
    if (AjaxService.isDebug) {
      console.log("URL : ", AjaxService.CONTEXT_PATH + url);
      console.log("Params : ", body);
    }
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${this.user.currentUserValue.token}`
      })
    };
    let errorFn = this.handleError;
    if (error) {
      errorFn = error;
    }

    return this.httpClient
      .post(AjaxService.CONTEXT_PATH + url, body, httpOptions )
      .toPromise()
      .then(success)
      .catch(errorFn);
  }

  download(url: string) {
    // let full_url = AjaxService.CONTEXT_PATH + url;
    let full_url = AjaxService.CONTEXT_PATH_EXPORT + url;
    window.open(full_url, 'Download');
  }

  downloadfileInajax(url: string) {
    window.location.href = AjaxService.CONTEXT_PATH + url;
  }

  doPostLogin(url: string, body: any) {
    this.globals.loading = true;
    if (AjaxService.isDebug) {
      console.log("URL : ", url);
      console.log("Params : ", body);
    }
    console.log("login", AjaxService.CONTEXT_PATH_LOGIN+url);
    return this.httpClient.post(AjaxService.CONTEXT_PATH_LOGIN+url, body).pipe(
      map((response: any) => {
        this.globals.loading = false;
        return response;
      }),
      catchError((err, caught) => {
        this.globals.loading = false;
        console.error("Message Error => ", err, caught);

        return throwError(err);
      })
    );
  }
}
